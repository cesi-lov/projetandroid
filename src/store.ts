import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    projects: [
      {
        title: "Projet de mise en relation freelance et entreprises",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec gravida id orci non faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam consequat varius lectus. Duis porttitor magna molestie elit bibendum sodales. Sed semper ligula et ante varius, vel fringilla eros accumsan. In lectus libero, vestibulum eget lacus molestie, ullamcorper feugiat nibh. Phasellus tortor enim, scelerisque vitae fringilla at, facilisis tempus mauris. Fusce ac odio lectus. Aenean massa justo, tincidunt a feugiat in, fringilla a neque. Nunc non tristique quam. Pellentesque porta fringilla eleifend. In hac habitasse platea dictumst. Pellentesque nisl metus, convallis tincidunt lacus pellentesque, placerat porttitor odio. Aliquam pulvinar mauris ac urna euismod lobortis. Ut erat nisl, dictum facilisis dui sed, mollis imperdiet elit. Nunc tempor justo ut sem dignissim maximus. In ac elit sapien. Etiam volutpat ultricies nisi nec lobortis. Fusce tincidunt dignissim tortor, a dapibus ligula porta eu. Nunc ipsum lectus, vehicula non facilisis a, facilisis quis lacus. Mauris vel sapien at nisi imperdiet varius. Proin in elit ac orci ultrices finibus. Pellentesque quis mattis massa, vel convallis tellus. Aliquam laoreet et arcu ut blandit. Integer nec sagittis lacus, sit amet rhoncus quam. Praesent viverra luctus arcu in feugiat. Cras tincidunt accumsan ex ut posuere. Praesent odio eros, euismod vel felis ac, suscipit pulvinar leo. In hac habitasse platea dictumst.",
        id: 0,
      },
      {
        title: "Développement site web",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec gravida id orci non faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam consequat varius lectus. Duis porttitor magna molestie elit bibendum sodales. Sed semper ligula et ante varius, vel fringilla eros accumsan. In lectus libero, vestibulum eget lacus molestie, ullamcorper feugiat nibh. Phasellus tortor enim, scelerisque vitae fringilla at, facilisis tempus mauris. Fusce ac odio lectus. Aenean massa justo, tincidunt a feugiat in, fringilla a neque. Nunc non tristique quam. Pellentesque porta fringilla eleifend. In hac habitasse platea dictumst. Pellentesque nisl metus, convallis tincidunt lacus pellentesque, placerat porttitor odio. Aliquam pulvinar mauris ac urna euismod lobortis. Ut erat nisl, dictum facilisis dui sed, mollis imperdiet elit. Nunc tempor justo ut sem dignissim maximus. In ac elit sapien. Etiam volutpat ultricies nisi nec lobortis. Fusce tincidunt dignissim tortor, a dapibus ligula porta eu. Nunc ipsum lectus, vehicula non facilisis a, facilisis quis lacus. Mauris vel sapien at nisi imperdiet varius. Proin in elit ac orci ultrices finibus. Pellentesque quis mattis massa, vel convallis tellus. Aliquam laoreet et arcu ut blandit. Integer nec sagittis lacus, sit amet rhoncus quam. Praesent viverra luctus arcu in feugiat. Cras tincidunt accumsan ex ut posuere. Praesent odio eros, euismod vel felis ac, suscipit pulvinar leo. In hac habitasse platea dictumst.",
        id: 1,
      },
      {
        title: "Maintenance wordpress",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec gravida id orci non faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam consequat varius lectus. Duis porttitor magna molestie elit bibendum sodales. Sed semper ligula et ante varius, vel fringilla eros accumsan. In lectus libero, vestibulum eget lacus molestie, ullamcorper feugiat nibh. Phasellus tortor enim, scelerisque vitae fringilla at, facilisis tempus mauris. Fusce ac odio lectus. Aenean massa justo, tincidunt a feugiat in, fringilla a neque. Nunc non tristique quam. Pellentesque porta fringilla eleifend. In hac habitasse platea dictumst. Pellentesque nisl metus, convallis tincidunt lacus pellentesque, placerat porttitor odio. Aliquam pulvinar mauris ac urna euismod lobortis. Ut erat nisl, dictum facilisis dui sed, mollis imperdiet elit. Nunc tempor justo ut sem dignissim maximus. In ac elit sapien. Etiam volutpat ultricies nisi nec lobortis. Fusce tincidunt dignissim tortor, a dapibus ligula porta eu. Nunc ipsum lectus, vehicula non facilisis a, facilisis quis lacus. Mauris vel sapien at nisi imperdiet varius. Proin in elit ac orci ultrices finibus. Pellentesque quis mattis massa, vel convallis tellus. Aliquam laoreet et arcu ut blandit. Integer nec sagittis lacus, sit amet rhoncus quam. Praesent viverra luctus arcu in feugiat. Cras tincidunt accumsan ex ut posuere. Praesent odio eros, euismod vel felis ac, suscipit pulvinar leo. In hac habitasse platea dictumst.",
        id: 3,
      },
    ]
  },
  mutations: {

  },
  actions: {

  },
});
